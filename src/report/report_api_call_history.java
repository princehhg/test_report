package report;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class report_api_call_history {

    public static void main(String[] args){
		
		Connection conn = null;
		PreparedStatement s = null;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
                        conn = DriverManager.getConnection("jdbc:mysql://dev.promptnow.com:41011/aeon","aeon","aeon1234");
                        
                        Scanner Sc = new Scanner(System.in);
                        System.out.print("MONTH : ");
                        int m = Sc.nextInt();
                        System.out.print("YEAR : ");
                        int y = Sc.nextInt();
			
                        String sql = "select service_id,count(call_datetime) as total "                                
                                + "from aeon.api_call_history "
                                + "where MONTH(call_datetime) = "+m+" and YEAR(call_datetime) = "+y+" "
                                + "group by service_id" ;
                        
                        s = conn.prepareStatement(sql);
			ResultSet rec = s.executeQuery();
			String path = "report_api_call_history.csv";
			FileWriter writer;

			try {
				File file = new File(path);
				writer = new FileWriter(file, true);
                                        writer.write("service_id");
                                        writer.write(",");
                                        writer.write("total");
					writer.write("\r\n");

				while((rec!=null) && (rec.next()))
	            {
					writer.write(rec.getString("service_id"));
					writer.write(",");
					writer.write(rec.getString("total"));
					writer.write("\r\n");
	            }
				writer.close();
				
				System.out.println("Write success!");
				
			} catch (IOException e) {
			}
		} catch (ClassNotFoundException | SQLException e) {
		}
		try {
			if(conn != null){
				s.close();
				conn.close();
			}
		} catch (SQLException e) {
		}
	}
}