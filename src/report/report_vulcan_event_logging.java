package report;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class report_vulcan_event_logging {

        public static void main(String[] args) {

            Connection connect = null;
            PreparedStatement pre = null;
            
            try {
                    Class.forName("com.mysql.jdbc.Driver");
                    connect = DriverManager.getConnection("jdbc:mysql://dev.promptnow.com:41011/aeon", "aeon", "aeon1234");
                      
                    Scanner scan =new Scanner(System.in);
                        System.out.print("MONTH :");
                        int M = scan.nextInt();
                        System.out.print("YEAR :");
                        int Y = scan.nextInt();
                    
                    String sql = "SELECT log_keyword,count(log_datetime) as Total "
                        + "FROM aeon.vulcan_event_logging "
                        + "WHERE MONTH(log_datetime ) ="+M+" AND  YEAR(log_datetime)= "+Y+" "
                        + "GROUP BY log_keyword"    ;  
                    pre = connect.prepareStatement(sql);                
                    ResultSet rec = pre.executeQuery();
                    String path = "reportlogkeyword.csv";
                    FileWriter writer;
                    
                    try {             
                        File file = new File(path);
                        writer = new FileWriter(file, true); 
                        writer.write("log_Keyword");
                        writer.write(",");
                        writer.write("Total");
                        writer.write("\r");
                        
                        while((rec != null) && (rec.next()))
                {                          
                            writer.write(rec.getString("log_keyword"));
                            writer.write(",");
                            writer.write(rec.getString("Total"));
                            writer.write("\r");                            
                }
                        writer.close();
                        System.out.println("sucess");
                    } catch (IOException e) {
                    }
                } catch (ClassNotFoundException | SQLException e) {
                    e.printStackTrace();
                }
                
                try {
                        if (connect != null) {
                                pre.close();
                                connect.close();
                        }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }     
}